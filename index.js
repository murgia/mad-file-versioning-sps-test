const fs = require('fs');
const got = require('got');
const Diff = require('diff');
const FormData = require('form-data');
const util = require('util');
const {HTTPError} = require("got");
const exec = util.promisify(require('child_process').exec);

// Get parameters from the arguments
let [authentication, machineId, versionId] = process.argv.slice(2);
machineId = machineId ? parseInt(machineId) : undefined;
versionId = versionId ? parseInt(versionId) : undefined;

// Prepare login request body
const [login, password] = Buffer
    .from(authentication, 'base64')
    .toString('binary')
    .split(':');
const body = JSON.stringify({login, password});

const defaultRequestOptions = {
    protocol: 'https:',
    hostname: 'layout.cern.ch',
    method: 'GET',
    headers: {},
    followRedirect: false,
    rejectUnauthorized: false,
};

const loginOptions = {
    ...defaultRequestOptions,
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    path: '/api/acw/login',
    body
};

const metadataRequestOptions = {
    ...defaultRequestOptions,
    path: '/api/v1/mad-file-gen'
};

const notifyRequestOptions = {
    ...defaultRequestOptions,
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    path: '/api/v1/tools/notify-mad-sequence-users'
};

function uploadTfsFileToLayout(option, Cookie) {
    const dir = "files/" + option.machineCode + "/SURVEY_" + option.versionLabel + "/";
    fs.readdir(dir, async (err, files) => {
        for (const file of files) {
            let beam = file.includes("b1") ? "B1" : file.includes("b2") ? "B2" : "BEAM";
            beam += file.includes("mid") ? "_MED" : "";
            const formData = new FormData();
            formData.append("file", fs.createReadStream(dir + file));
            formData.append("version", option.versionLabel)
            formData.append("machine-code", option.machineCode)
            formData.append("beam", beam)
            const tfsUploadRequestOptions = {
                ...defaultRequestOptions,
                method: 'POST',
                headers: {Cookie},
                path: '/api/v1/tfs-elements',
                body: formData
            };
            console.log(`Uploading [${file}] for [${option.machineCode}] [${option.versionLabel}] to Layout`)
            try {
                await got(tfsUploadRequestOptions);
            } catch (error) {
                console.log(`Error while uploading [${file}] for [${option.machineCode}] [${option.versionLabel}]`, error)
                process.exit(1)
            }
        }
    })
}

(async () => {
    try {
        const loginResponse = await got(loginOptions);
        const Cookie = loginResponse.headers['set-cookie']
            .filter(x => !x.includes('acw_token=;'))
            .join(';');
        const changes = [];

        metadataRequestOptions.headers.Cookie = Cookie;
        notifyRequestOptions.headers.Cookie = Cookie;

        const metadataResponse = await got(metadataRequestOptions);
        let metadata = JSON.parse(metadataResponse.body);
        for (const option of metadata) {
            // Update only the specified machine when the build is parameterized
            if (machineId && versionId && (option.machineId !== machineId || option.versionId !== versionId))
                continue;
            const refreshRequestOptions = {
                ...defaultRequestOptions,
                method: 'POST',
                headers: {Cookie},
                path: `/api/v1/tools/refresh-mad-sequences?machineId=${option.machineId}&versionId=${option.versionId}`
            };

            console.log(`Refreshing '${option.machineCode}-${option.versionLabel}'...`);
            try {
                await got(refreshRequestOptions);
            } catch (error) {
                console.error(`Error while refreshing '${option.machineCode}-${option.versionLabel}'!`, error);
                continue;
            }
            console.log(`'${option.machineCode}-${option.versionLabel}' refreshed!`);

            const fileRequestOptions = fileType => ({
                ...defaultRequestOptions,
                headers: {Cookie},
                path: `/api/v1/mad-file-gen/${option.machineId}?fileType=${fileType}&versionId=${option.versionId}`,
            });

            for (const fileType of ['APERTURE', 'STANDARD']) {
                const filePrefix = fileType === 'STANDARD' ? '' : 'APERTURE_';
                console.log(`Downloading file '${filePrefix}${option.machineCode}-${option.versionLabel}'...`);
                try {
                    const fileResponse = await got(fileRequestOptions(fileType));

                    // Create the files directory when it doesn't exist
                    if (!fs.existsSync(`./files`)) {
                        fs.mkdirSync(`./files`);
                        console.log(`The directory 'files' was created!`);
                    }

                    // Create the machine directory when it doesn't exist
                    if (!fs.existsSync(`./files/${option.machineCode}`)) {
                        fs.mkdirSync(`./files/${option.machineCode}`);
                        console.log(`The directory '${option.machineCode}' was created!`);
                    }

                    const fileName = `./files/${option.machineCode}/${filePrefix}${option.versionLabel}.seq`;
                    const oldFile = fs.existsSync(fileName) ? fs.readFileSync(fileName, 'utf8') : '';
                    fs.writeFileSync(fileName, fileResponse.body);
                    console.log(`The file '${filePrefix}${option.machineCode}-${option.versionLabel}' was saved!`);

                    let validationError = null;

                    if (fileType === 'STANDARD') {
                        const validationFile = `./files/${option.machineCode}/tests/test_load_seq.py`;

                        if (!fs.existsSync(validationFile)) {
                            console.log(`No validation file: ${validationFile} - SKIPPING`);
                        } else {
                            console.log(`Starting MAD validation process for '${option.machineCode}-${option.versionLabel}'`);
                            const cmd = `python3 ${validationFile} "${fileName}"`;
                            console.log(cmd)
                            await exec(cmd)
                                .then(({stdout})=> {
                                    validationError = stdout;
                                    console.log(`Validation: ${validationError === null ? 'NONE' : validationError || 'OK!'}`)
                                })
                                .catch(e => {
                                    validationError = `${e.message} \n stdout:${e.stdout}`;
                                    console.log(validationError)
                                });
                        }
                    }
                    // Check if the file has changes
                    const diff = Diff.diffTrimmedLines(oldFile, fileResponse.body);
                    const changesCount = diff.filter(change => change.added || change.removed).length;
                    if (changesCount > 2 || validationError) {
                        changes.push({...option, fileType, error: validationError});
                    }
                } catch (error) {
                    if (error.response && error.response.statusCode === 404) {
                        console.log(`File type ${fileType} does not exist for '${option.machineCode}-${option.versionLabel}'`)
                    } else {
                        console.error(`Error while downloading file '${filePrefix}${option.machineCode}-${option.versionLabel}'!`, error);
                    }
                }
            }
            if (option.madSurveyGeneration === "Y") {
                uploadTfsFileToLayout(option, Cookie);
            }
        }

        // Send changes notification
        const body = JSON.stringify(changes);
        await got({...notifyRequestOptions, body});
    } catch (error) {
        if (error instanceof HTTPError) {
            console.log(`Got error code ${error.response.statusCode} from HTTP Request to: ${error.request.requestUrl}`, error)
        } else {
            console.error(error);
        }
    }
})();
