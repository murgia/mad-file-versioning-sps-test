FROM registry.cern.ch/acc/acc-py_cc7_ci:pro

RUN pip install --no-cache cpymad
RUN yum install -y nodejs npm

# Configure Git
RUN git config --global user.name becodev && \
    git config --global user.email layout-dev-team@cern.ch
