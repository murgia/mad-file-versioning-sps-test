

  /**********************************************************************************
  *
  * DI version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 23-SEP-2023 02:10:42 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.DI_BCT                       := 0;
l.DI_BTVPF                     := 0.27;
l.DI_BTVPL122                  := 0.56;
l.DI_BTVPL126                  := 0.7;
l.DI_BTVPL128                  := 0;
l.DI_MBHEDWWP                  := 1.461;
l.DI_MBHFECIP                  := 1.59;
l.DI_MCCBAWCC                  := 0.6;
l.DI_MCXABCIP                  := 0.4;
l.DI_MQNEETWC                  := 1;
l.DI_MQNEQNWP                  := 0.672;
l.DI_MQNERNWP                  := 0.7396;
l.DI_MQNFGFWP                  := 0.92;
l.DI_MQNFHFWP                  := 0.92;
l.DI_MSSMI01A                  := 1.4;
l.DI_OMK                       := 0;
l.DI_TCMAD001                  := 1.6;
l.DI_TC___001                  := 0.4;
l.DI_THOR_001                  := 0;
l.DI_TSL__001                  := 0.202;

//---------------------- COLLIMATOR     ---------------------------------------------
DI_TCMAD001    : COLLIMATOR  , L := l.DI_TCMAD001;       ! Collimation Mask for AD Target
DI_TC___001    : COLLIMATOR  , L := l.DI_TC___001;       ! Collimator Line DI
DI_THOR_001    : COLLIMATOR  , L := l.DI_THOR_001;       ! AD Target, Horn, variant 001
//---------------------- CORRECTOR      ---------------------------------------------
DI_MCCBAWCC    : CORRECTOR   , L := l.DI_MCCBAWCC;       ! Corrector magnet, H+V, steering ACOL
DI_MCXABCIP    : CORRECTOR   , L := l.DI_MCXABCIP;       ! Corrector magnet, H or V, type MCVA
//---------------------- MARKER         ---------------------------------------------
DI_OMK         : MARKER      , L := l.DI_OMK;            ! DI markers
//---------------------- MONITOR        ---------------------------------------------
DI_BCT         : MONITOR     , L := l.DI_BCT;            ! Beam current transformer
DI_BTVPF       : MONITOR     , L := l.DI_BTVPF;          ! Beam Observation TV, Pneumatic linear screen actuator type F
DI_BTVPL122    : MONITOR     , L := l.DI_BTVPL122;       ! Beam observation TV Pneumatic Linear, variant 122
DI_BTVPL126    : MONITOR     , L := l.DI_BTVPL126;       ! Beam observation TV Pneumatic Linear, variant 126
DI_BTVPL128    : MONITOR     , L := l.DI_BTVPL128;       ! Beam observation TV Pneumatic Linear, variant 128
DI_TSL__001    : MONITOR     , L := l.DI_TSL__001;       ! Slit Horizontal Line DI
//---------------------- QUADRUPOLE     ---------------------------------------------
DI_MQNEETWC    : QUADRUPOLE  , L := l.DI_MQNEETWC;       ! Quadrupole magnet, type Q100, 1m - Magnetic length taken from EDMS 1786085
DI_MQNEQNWP    : QUADRUPOLE  , L := l.DI_MQNEQNWP;       ! Quadrupole magnet, type QN AD injection line
DI_MQNERNWP    : QUADRUPOLE  , L := l.DI_MQNERNWP;       ! Quadrupole magnet, tpye QN AD injection
DI_MQNFGFWP    : QUADRUPOLE  , L := l.DI_MQNFGFWP;       ! Quadrupole magnet, tpye QD1 AD injection
DI_MQNFHFWP    : QUADRUPOLE  , L := l.DI_MQNFHFWP;       ! Quadrupole magnet, tpye QF2 AD injection
//---------------------- RBEND          ---------------------------------------------
DI_MBHEDWWP    : RBEND       , L := l.DI_MBHEDWWP;       ! Bending magnet, 1type W
DI_MBHFECIP    : RBEND       , L := l.DI_MBHFECIP;       ! Bending magnet, AD target
//---------------------- SBEND          ---------------------------------------------
DI_MSSMI01A    : SBEND       , L := l.DI_MSSMI01A;       ! Magnetic Septum, Injection - type 01A (SMI5306)


/************************************************************************************/
/*                      DI SEQUENCE                                                 */
/************************************************************************************/

DI : SEQUENCE, refer = centre,          L = 47.62053;
 FTA.TAR9065                   : DI_OMK          , at = .0000005     , slot_id = 53455378;
 DI.HOR6000                    : DI_THOR_001     , at = .3660005     , slot_id = 53459076;
 DI.COL6005                    : DI_TCMAD001     , at = 2.4326       , slot_id = 47489135;
 DI.BCT6006                    : DI_BCT          , at = 3.29135      , slot_id = 55408632;
 DI.BTV6008                    : DI_BTVPF        , at = 3.6126       , slot_id = 53455460;
 DI.QDE6010                    : DI_MQNFGFWP     , at = 4.888883     , slot_id = 14237185;
 DI.QFO6020                    : DI_MQNFHFWP     , at = 6.088882     , slot_id = 14237187;
 DI.BHZ6024                    : DI_MBHFECIP     , at = 7.566486     , slot_id = 14237179;
 DI.BHZ6025                    : DI_MBHFECIP     , at = 9.31446      , slot_id = 14237180;
 DI.BTV6028                    : DI_BTVPL122     , at = 11.812492    , slot_id = 53455469;
 DI.QDE6030                    : DI_MQNEQNWP     , at = 12.696       , slot_id = 14237182;
 DI.BHZ6034                    : DI_MBHEDWWP     , at = 14.043169    , slot_id = 14237174;
 DI.BHZ6035                    : DI_MBHEDWWP     , at = 15.793166    , slot_id = 14237175;
 DI.SLH6038                    : DI_TSL__001     , at = 16.7326      , slot_id = 57618392;
 DI.QFO6040                    : DI_MQNEQNWP     , at = 17.223191    , slot_id = 14237183;
 DI.COH6042                    : DI_TC___001     , at = 17.832591    , slot_id = 57618502;
 DI.BHZ6044                    : DI_MBHEDWWP     , at = 18.873206    , slot_id = 14237176;
 DI.BHZ6045                    : DI_MBHEDWWP     , at = 20.664884    , slot_id = 14237177;
 DI.BTV6048                    : DI_BTVPL122     , at = 21.93812     , slot_id = 53455478;
 DI.QDE6050                    : DI_MQNEQNWP     , at = 25.956118    , slot_id = 55408753;
 DI.BCT6052                    : DI_BCT          , at = 27.525       , slot_id = 55408641;
 DI.QFO6060                    : DI_MQNEQNWP     , at = 29.665721    , slot_id = 55408762;
 DI.BHZ6064                    : DI_MBHEDWWP     , at = 30.960346    , slot_id = 55408696;
 DI.BHZ6065                    : DI_MBHEDWWP     , at = 32.665471    , slot_id = 55408705;
 DI.DVT6067                    : DI_MCXABCIP     , at = 34.170307    , slot_id = 55408726;
 DI.BTV6068                    : DI_BTVPL126     , at = 34.920309    , slot_id = 53455487;
 DI.QDE6070                    : DI_MQNEETWC     , at = 37.733567    , slot_id = 55408735;
 DI.QFO6080                    : DI_MQNEETWC     , at = 41.11357     , slot_id = 55408744;
 DI.DVTH6081                   : DI_MCCBAWCC     , at = 42.41357     , slot_id = 55408717;
 DR.QDC53                      : DI_MQNERNWP     , at = 45.759205    , slot_id = 6731474;
 DI.SMI5306                    : DI_MSSMI01A     , at = 47.459376    , slot_id = 57525202;
 DR.BTV5308                    : DI_BTVPL128     , at = 48.351687    , slot_id = 57952475;
ENDSEQUENCE;

return;