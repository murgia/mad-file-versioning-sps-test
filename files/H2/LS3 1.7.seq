

  /**********************************************************************************
  *
  * H2 version (draft) LS3 1.7 in MAD X SEQUENCE format
  * Generated the 06-MAR-2023 18:23:26 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.H2_BXMWPCS01                 := 0.13;
l.H2_BXSCI                     := 0.2;
l.H2_LSE                       := 0.74;
l.H2_MBHHEHWC                  := 2;
l.H2_MBNH_HWP                  := 5;
l.H2_MBNV_HWP                  := 5;
l.H2_MBXFACWP                  := 1;
l.H2_MCXCAHWC                  := 0.4;
l.H2_MQNEETWC                  := 1;
l.H2_MQNFBTWC                  := 2;
l.H2_MSN                       := 3.2;
l.H2_MTN                       := 3.6;
l.H2_QNL__8WP                  := 2.99;
l.H2_QSL__8WP                  := 3;
l.H2_QWL__8WP                  := 2.948;
l.H2_TBACA                     := 0.5;
l.H2_TBID                      := 0.25;
l.H2_TCMAA                     := 0.4;
l.H2_XCEDN                     := 6.317;
l.H2_XCET_001                  := 0;
l.H2_XCHV_001                  := 1;
l.H2_XCIOB001                  := 0.1;
l.H2_XCSH_001                  := 1;
l.H2_XCSV_001                  := 1;
l.H2_XSCI                      := 0.1;
l.H2_XTAX_001                  := 1.615;
l.H2_XTAX_002                  := 1.615;

//---------------------- COLLIMATOR     ---------------------------------------------
H2_TCMAA       : COLLIMATOR  , L := l.H2_TCMAA;          ! Collimation mask type A
H2_XCEDN       : COLLIMATOR  , L := l.H2_XCEDN;          ! Cherenkov Differential Counter Nord
H2_XCHV_001    : COLLIMATOR  , L := l.H2_XCHV_001;       ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
H2_XCIOB001    : COLLIMATOR  , L := l.H2_XCIOB001;       ! Converter IN OUT Block - Steel 1.5m
H2_XCSH_001    : COLLIMATOR  , L := l.H2_XCSH_001;       ! SPS Collimator a fente horizontal (design 1970)
H2_XCSV_001    : COLLIMATOR  , L := l.H2_XCSV_001;       ! Collimator 2 blocks vertical (design 1970)
//---------------------- HKICKER        ---------------------------------------------
H2_MSN         : HKICKER     , L := l.H2_MSN;            ! Septum magnet, north area - offset mechanical and optical dimensions according to drawing 1766138 (not well indicated)
//---------------------- INSTRUMENT     ---------------------------------------------
H2_TBACA       : INSTRUMENT  , L := l.H2_TBACA;          ! Target Box variant A
H2_TBID        : INSTRUMENT  , L := l.H2_TBID;           ! target beam instrumentation, downstream
H2_XCET_001    : INSTRUMENT  , L := l.H2_XCET_001;      
H2_XTAX_001    : INSTRUMENT  , L := l.H2_XTAX_001;       ! Target Absorber Type 001
H2_XTAX_002    : INSTRUMENT  , L := l.H2_XTAX_002;       ! Target Absorber Type 002
//---------------------- KICKER         ---------------------------------------------
H2_MCXCAHWC    : KICKER      , L := l.H2_MCXCAHWC;       ! Corrector magnet, H or V, type MDX
//---------------------- MONITOR        ---------------------------------------------
H2_BXMWPCS01   : MONITOR     , L := l.H2_BXMWPCS01;      ! Multi-Wire Proportional Chamber - Detector - Small
H2_BXSCI       : MONITOR     , L := l.H2_BXSCI;          ! Scintillator Counter Detector (Intensity Monitor)
H2_XSCI        : MONITOR     , L := l.H2_XSCI;           ! Ensemble: Cadre et Scintillateur BXSCI
//---------------------- QUADRUPOLE     ---------------------------------------------
H2_MQNEETWC    : QUADRUPOLE  , L := l.H2_MQNEETWC;       ! Quadrupole magnet, type Q100, 1m - Magnetic length taken from EDMS 1786085
H2_MQNFBTWC    : QUADRUPOLE  , L := l.H2_MQNFBTWC;       ! Quadrupole magnet, type Q200, 2m
H2_QNL__8WP    : QUADRUPOLE  , L := l.H2_QNL__8WP;       ! Quadrupole, secondary beams, type north area
H2_QSL__8WP    : QUADRUPOLE  , L := l.H2_QSL__8WP;       ! Quadrupole, slim, long, - Same magnet type SPQSLD_
H2_QWL__8WP    : QUADRUPOLE  , L := l.H2_QWL__8WP;       ! Quadrupole, Secondary Beams, West Area Type
//---------------------- RBEND          ---------------------------------------------
H2_MBHHEHWC    : RBEND       , L := l.H2_MBHHEHWC;       ! Bending magnet, type M200, straight poles
H2_MBNH_HWP    : RBEND       , L := l.H2_MBNH_HWP;       ! Bending magnet, secondary beams, horizontal, north area
H2_MBNV_HWP    : RBEND       , L := l.H2_MBNV_HWP;       ! Bending magnet, secondary beams, vertical, north area - Mechanical dimensions from NORMA
H2_MBXFACWP    : RBEND       , L := l.H2_MBXFACWP;       ! Bending Magnet, H or V, type VB3, 1m gap 108mm - Magnetic length according to EDMS 1786052
H2_MTN         : RBEND       , L := l.H2_MTN;            ! Bending magnet, Target N
//---------------------- SEXTUPOLE      ---------------------------------------------
H2_LSE         : SEXTUPOLE   , L := l.H2_LSE;            ! Sextupole lens, extraction


/************************************************************************************/
/*                       H2 SEQUENCE                                                */
/************************************************************************************/

H2 : SEQUENCE, refer = centre,          L = 689.863;
 TBACA.X0200000                : H2_TBACA        , at = 0            , slot_id = 56309981;
 TBID.230950                   : H2_TBID         , at = .475         , slot_id = 46651909;
 TCMAA.X0200001                : H2_TCMAA        , at = .85          , slot_id = 56992098;
 MTN.X0200003                  : H2_MTN          , at = 3.15         , slot_id = 56477726;
 MTN.X0200007                  : H2_MTN          , at = 7.35         , slot_id = 56477749;
 XTAX.X0210023                 : H2_XTAX_001     , at = 22.8075      , slot_id = 56310124;
 XTAX.X0210025                 : H2_XTAX_002     , at = 24.4325      , slot_id = 56310147;
 XCIO.X0210026                 : H2_XCIOB001     , at = 25.325       , slot_id = 56310275;
 MSN.X0210027                  : H2_MSN          , at = 27.25        , slot_id = 56310535;
 MSN.X0210031                  : H2_MSN          , at = 30.8         , slot_id = 56310558;
 QNL.X0210038                  : H2_QNL__8WP     , at = 37.72        , slot_id = 56048689;
 QSL.X0210041                  : H2_QSL__8WP     , at = 41.45        , slot_id = 56310604;
 XCSH.X0210045                 : H2_XCSH_001     , at = 45.145       , slot_id = 56051803;
 XCSV.X0210046                 : H2_XCSV_001     , at = 46.43        , slot_id = 56271727;
 QNL.X0210049                  : H2_QNL__8WP     , at = 48.72        , slot_id = 56048546;
 MBNV.X0210053                 : H2_MBNV_HWP     , at = 53.265       , slot_id = 56310696;
 MBNV.X0210059                 : H2_MBNV_HWP     , at = 58.925       , slot_id = 56310733;
 MBNV.X0210065                 : H2_MBNV_HWP     , at = 64.585       , slot_id = 56310756;
 XCHV.X0210068                 : H2_XCHV_001     , at = 67.99        , slot_id = 56051629;
 MBNV.X0210071                 : H2_MBNV_HWP     , at = 71.395       , slot_id = 56310793;
 MBNV.X0210077                 : H2_MBNV_HWP     , at = 77.055       , slot_id = 56310816;
 MBNV.X0210083                 : H2_MBNV_HWP     , at = 82.715       , slot_id = 56310840;
 QNL.X0210087                  : H2_QNL__8WP     , at = 87.26        , slot_id = 56048555;
 QNL.X0210099                  : H2_QNL__8WP     , at = 98.608       , slot_id = 56048581;
 XCHV.X0210132                 : H2_XCHV_001     , at = 132.135      , slot_id = 56051638;
 QWL.X0210134                  : H2_QWL__8WP     , at = 134.455      , slot_id = 56056491;
 LSE.X0210137                  : H2_LSE          , at = 136.75       , slot_id = 56056545;
 MCXCA.X0210138                : H2_MCXCAHWC     , at = 137.712      , slot_id = 56032763;
 MCXCA.X0210139                : H2_MCXCAHWC     , at = 138.482      , slot_id = 56032772;
 QNL.X0210170                  : H2_QNL__8WP     , at = 170.242      , slot_id = 56048594;
 QNL.X0210182                  : H2_QNL__8WP     , at = 181.59       , slot_id = 56048612;
 XCSV.X0210184                 : H2_XCSV_001     , at = 183.88       , slot_id = 56271736;
 XCHV.X0210201                 : H2_XCHV_001     , at = 200.86       , slot_id = 56051647;
 MCXCA.X0210202                : H2_MCXCAHWC     , at = 201.87       , slot_id = 56032781;
 XCSV.X0210218                 : H2_XCSV_001     , at = 217.84       , slot_id = 56400585;
 QNL.X0210220                  : H2_QNL__8WP     , at = 220.13       , slot_id = 56048621;
 QNL.X0210231                  : H2_QNL__8WP     , at = 231.478      , slot_id = 56048603;
 XSCI.X0210264                 : H2_XSCI         , at = 264.508      , slot_id = 56507094;
 QWL.X0210267                  : H2_QWL__8WP     , at = 267.325      , slot_id = 56056523;
 QNL.X0210303                  : H2_QNL__8WP     , at = 303.112      , slot_id = 56048630;
 QNL.X0210314                  : H2_QNL__8WP     , at = 314.459      , slot_id = 56048503;
 MBNV.X0210319                 : H2_MBNV_HWP     , at = 319.005      , slot_id = 56414790;
 MBNV.X0210325                 : H2_MBNV_HWP     , at = 324.665      , slot_id = 56414808;
 MBNV.X0210330                 : H2_MBNV_HWP     , at = 330.325      , slot_id = 56414817;
 MBNV.X0210337                 : H2_MBNV_HWP     , at = 337.135      , slot_id = 56414826;
 MBNV.X0210343                 : H2_MBNV_HWP     , at = 342.795      , slot_id = 56414835;
 MBNV.X0210348                 : H2_MBNV_HWP     , at = 348.455      , slot_id = 56414844;
 QNL.X0210353                  : H2_QNL__8WP     , at = 353          , slot_id = 56048519;
 QNL.X0210364                  : H2_QNL__8WP     , at = 364.348      , slot_id = 56048639;
 XWCA.X021386                  : H2_BXMWPCS01    , at = 385.21       , slot_id = 56883794;
 XSCI.X021386                  : H2_BXSCI        , at = 385.29       , slot_id = 56883733;
 XCHV.X0210398                 : H2_XCHV_001     , at = 397.815      , slot_id = 56051683;
 QNL.X0210400                  : H2_QNL__8WP     , at = 400.165      , slot_id = 56048528;
 MCXCA.X0210402                : H2_MCXCAHWC     , at = 402.315      , slot_id = 56032790;
 MBNH.X0210405                 : H2_MBNH_HWP     , at = 405.48       , slot_id = 56421957;
 XCSH.X0210429                 : H2_XCSH_001     , at = 429.165      , slot_id = 56051812;
 MBXFA.X0210440                : H2_MBXFACWP     , at = 439.71       , slot_id = 56032855;
 MBXFA.X0210443                : H2_MBXFACWP     , at = 442.705      , slot_id = 56032864;
 QNL.X0210447                  : H2_QNL__8WP     , at = 447.33       , slot_id = 56048537;
 QWL.X0210457                  : H2_QWL__8WP     , at = 457.079      , slot_id = 56056473;
 XCED.X0210463                 : H2_XCEDN        , at = 463.1915     , slot_id = 56425516;
 QWL.X0210470                  : H2_QWL__8WP     , at = 469.547      , slot_id = 56056482;
 QNL.X0210479                  : H2_QNL__8WP     , at = 479.296      , slot_id = 56048650;
 QNL.X0210489                  : H2_QNL__8WP     , at = 488.856      , slot_id = 56048659;
 MCXCA.X0210491                : H2_MCXCAHWC     , at = 491.006      , slot_id = 56032804;
 MCXCA.X0210492                : H2_MCXCAHWC     , at = 491.776      , slot_id = 56032813;
 QNL.X0210500                  : H2_QNL__8WP     , at = 501.656      , slot_id = 56048671;
 XCET.X0210507                 : H2_XCET_001     , at = 507.3        , slot_id = 57389046;
 QNL.X0210511                  : H2_QNL__8WP     , at = 511.54       , slot_id = 56048680;
 QNL.X0210524                  : H2_QNL__8WP     , at = 521.446      , slot_id = 56048698;
 XSCI.X0210528                 : H2_XSCI         , at = 527.458      , slot_id = 57388996;
 MCXCA.X0210567                : H2_MCXCAHWC     , at = 567.468      , slot_id = 56032822;
 MCXCA.X0210568                : H2_MCXCAHWC     , at = 568.238      , slot_id = 56032846;
 MQNFB.X0210633                : H2_MQNFBTWC     , at = 633.293      , slot_id = 56032709;
 MQNFB.X0210636                : H2_MQNFBTWC     , at = 636.17       , slot_id = 56032718;
 MQNFB.X0210640                : H2_MQNFBTWC     , at = 639.616      , slot_id = 56032727;
 MBHHE.X0210643                : H2_MBHHEHWC     , at = 642.766      , slot_id = 56032681;
 MBHHE.X0210646                : H2_MBHHEHWC     , at = 646.264      , slot_id = 56024824;
 XCSH.X0210649                 : H2_XCSH_001     , at = 648.464      , slot_id = 56051842;
 MQNFB.X0210651                : H2_MQNFBTWC     , at = 650.542      , slot_id = 56032736;
 MBHHE.X0210654                : H2_MBHHEHWC     , at = 653.657      , slot_id = 56032700;
 MBHHE.X0210657                : H2_MBHHEHWC     , at = 657.379      , slot_id = 56032691;
 MQNEE.X0210661                : H2_MQNEETWC     , at = 661.227      , slot_id = 56425822;
 MQNEE.X0210663                : H2_MQNEETWC     , at = 663.64       , slot_id = 56425831;
ENDSEQUENCE;

return;