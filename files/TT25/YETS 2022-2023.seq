

  /**********************************************************************************
  *
  * TT25 version (draft) YETS 2022-2023 in MAD X SEQUENCE format
  * Generated the 22-DEC-2023 03:19:32 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TT25_BBS                     := 0.45;
l.TT25_BSGV_005                := 0;
l.TT25_BSI                     := 0;
l.TT25_BSM                     := 0.45;
l.TT25_BSPH_002                := 0;
l.TT25_BTV                     := 0;
l.TT25_MAL                     := 0;
l.TT25_MBB                     := 6.26;
l.TT25_MDAH                    := 0;
l.TT25_MDAV                    := 1.4;
l.TT25_MDLV                    := 1.4;
l.TT25_OMK                     := 0;
l.TT25_QNLF                    := 0;
l.TT25_QTAD                    := 0;
l.TT25_QTAF                    := 0;
l.TT25_QTLD                    := 2.99;
l.TT25_TBID                    := 0.25;
l.TT25_TBIU                    := 0;
l.TT25_TCMAB                   := 0.4;
l.TT25_TCMAF                   := 0.35;
l.TT25_VVSA                    := 0.175;

//---------------------- COLLIMATOR     ---------------------------------------------
TT25_TCMAB               : COLLIMATOR  , L := l.TT25_TCMAB;        ! Collimation mask type B
TT25_TCMAF               : COLLIMATOR  , L := l.TT25_TCMAF;        ! Collimation mask type F
//---------------------- INSTRUMENT     ---------------------------------------------
TT25_BBS                 : INSTRUMENT  , L := l.TT25_BBS;          ! Beam Box Scanner
TT25_BSGV_005            : INSTRUMENT  , L := l.TT25_BSGV_005;     ! SEM grid, vertical
TT25_BSI                 : INSTRUMENT  , L := l.TT25_BSI;          ! SEM intensity
TT25_BSM                 : INSTRUMENT  , L := l.TT25_BSM;          ! SEM position, moveable
TT25_BTV                 : INSTRUMENT  , L := l.TT25_BTV;          ! Beam observation TV with screen
TT25_TBIU                : INSTRUMENT  , L := l.TT25_TBIU;         ! target beam instrumentation, upstream
TT25_VVSA                : INSTRUMENT  , L := l.TT25_VVSA;         ! vacuum valve, sector, diameter 100 mm
//---------------------- MARKER         ---------------------------------------------
TT25_OMK                 : MARKER      , L := l.TT25_OMK;          ! TT25 markers
//---------------------- MONITOR        ---------------------------------------------
TT25_BSPH_002            : MONITOR     , L := l.TT25_BSPH_002;     ! SEM position, horizontal
TT25_TBID                : MONITOR     , L := l.TT25_TBID;         ! target beam instrumentation, downstream
//---------------------- QUADRUPOLE     ---------------------------------------------
TT25_QNLF                : QUADRUPOLE  , L := l.TT25_QNLF;         ! quadrupole, secondary beams, north area type, focussing
TT25_QTAD                : QUADRUPOLE  , L := l.TT25_QTAD;         ! quadrupole, BT line, long, enlarged aperture, defocussing
TT25_QTAF                : QUADRUPOLE  , L := l.TT25_QTAF;         ! quadrupole, BT line, long, enlarged aperture, focussing
TT25_QTLD                : QUADRUPOLE  , L := l.TT25_QTLD;         ! quadrupole, BT line, long, defocussing
//---------------------- RBEND          ---------------------------------------------
TT25_MAL                 : RBEND       , L := l.TT25_MAL;          ! bending magnet, long, type B340 (former antiproton transfer line)
TT25_MBB                 : RBEND       , L := l.TT25_MBB;          ! Bending Magnet, main, type B2
TT25_MDAH                : RBEND       , L := l.TT25_MDAH;         ! Correcting dipole, BT line, enlarged aperture, horizontal deflection
TT25_MDAV                : RBEND       , L := l.TT25_MDAV;         ! Correcting dipole, BT line, enlarged aperture, vertical deflection
TT25_MDLV                : RBEND       , L := l.TT25_MDLV;         ! Correcting dipole, BT line, long, vertical deflection


/************************************************************************************/
/*                      TT25 SEQUENCE                                               */
/************************************************************************************/

TT25 : SEQUENCE, refer = centre,        L = 257.9268;
 BSGV.250089                   : TT25_BSGV_005          , at = 29.6583      , slot_id = 47601662;
 MDLV.250090                   : TT25_MDLV              , at = 31.0633      , slot_id = 47601663;
 VVSA.250117                   : TT25_VVSA              , at = 46.5658      , slot_id = 57167737;
 BTV.250118                    : TT25_BTV               , at = 47.3279      , slot_id = 47601664;
 QTAD.250200                   : TT25_QTAD              , at = 49.4709      , slot_id = 47601665;
 BSPH.250217                   : TT25_BSPH_002          , at = 58.7229      , slot_id = 47601666;
 QTAF.250300                   : TT25_QTAF              , at = 60.9459      , slot_id = 47601667;
 QTAF.250400                   : TT25_QTAF              , at = 64.5319      , slot_id = 47601668;
 MDLV.250504                   : TT25_MDLV              , at = 126.6727     , slot_id = 47601669;
 BSGV.250508                   : TT25_BSGV_005          , at = 128.0857     , slot_id = 47601670;
 MBB.250509                    : TT25_MBB               , at = 131.8867     , slot_id = 47601671;
 MBB.250520                    : TT25_MBB               , at = 138.5267     , slot_id = 47601672;
 MBB.250531                    : TT25_MBB               , at = 145.1667     , slot_id = 47601673;
 MBB.250542                    : TT25_MBB               , at = 151.8067     , slot_id = 47601674;
 MBB.250553                    : TT25_MBB               , at = 158.4467     , slot_id = 47601675;
 MBB.250564                    : TT25_MBB               , at = 165.0867     , slot_id = 47601676;
 BSPH.250611                   : TT25_BSPH_002          , at = 193.068      , slot_id = 47601677;
 QTAF.250700                   : TT25_QTAF              , at = 195.211      , slot_id = 47601678;
 QTAF.250800                   : TT25_QTAF              , at = 198.877      , slot_id = 47601679;
 MDAH.250806                   : TT25_MDAH              , at = 201.805      , slot_id = 47601680;
 QTLD.250900                   : TT25_QTLD              , at = 211.877      , slot_id = 47601681;
 QTLD.251000                   : TT25_QTLD              , at = 215.543      , slot_id = 47601683;
 MDAV.251006                   : TT25_MDAV              , at = 218.471      , slot_id = 47601685;
 BSGV.251009                   : TT25_BSGV_005          , at = 219.884      , slot_id = 47601693;
 BSI.251010                    : TT25_BSI               , at = 220.584      , slot_id = 47601694;
 QNLF.251100                   : TT25_QNLF              , at = 228.543      , slot_id = 47601695;
 QNLF.251200                   : TT25_QNLF              , at = 232.209      , slot_id = 47601696;
 MAL.251208                    : TT25_MAL               , at = 237.5783     , slot_id = 47601697;
 MAL.251215                    : TT25_MAL               , at = 241.6553     , slot_id = 47601698;
 BSM.251221                    : TT25_BSM               , at = 243.9883     , slot_id = 47601699;
 BBS.251245                    : TT25_BBS               , at = 258.0668     , slot_id = 47601700;
 TCMAF.251246                  : TT25_TCMAF             , at = 258.7168     , slot_id = 57128956;
 TBIU.251247                   : TT25_TBIU              , at = 259.0668     , slot_id = 47601701;
 TBACA.X0600000                : TT25_OMK               , at = 259.5918     , slot_id = 56964656;
 TBID.251248                   : TT25_TBID              , at = 260.0668     , slot_id = 47601702;
 TCMAB.X0600001                : TT25_TCMAB             , at = 260.4418     , slot_id = 56992367;
ENDSEQUENCE;

return;