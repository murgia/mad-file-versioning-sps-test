

  /**********************************************************************************
  *
  * TT23 version (draft) YETS 2022-2023 in MAD X SEQUENCE format
  * Generated the 22-DEC-2023 03:18:32 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TT23_BSG                     := 0;
l.TT23_BSGV_005                := 0;
l.TT23_BSI                     := 0;
l.TT23_BSM                     := 0.45;
l.TT23_BSPH_002                := 0;
l.TT23_BTV                     := 0.45;
l.TT23_BTV__004                := 0;
l.TT23_MBNH_HWP                := 5;
l.TT23_MDAH                    := 0;
l.TT23_MDAV                    := 1.4;
l.TT23_MTN__HWP                := 3.6;
l.TT23_OMK                     := 0;
l.TT23_QTAD                    := 0;
l.TT23_QTAF                    := 0;
l.TT23_TBID                    := 0.25;
l.TT23_TBIU                    := 0;
l.TT23_TCMAA                   := 0.4;
l.TT23_TCMAE                   := 0.35;
l.TT23_VVSB                    := 0.175;

//---------------------- COLLIMATOR     ---------------------------------------------
TT23_TCMAA               : COLLIMATOR  , L := l.TT23_TCMAA;        ! Collimation mask type A
TT23_TCMAE               : COLLIMATOR  , L := l.TT23_TCMAE;        ! Collimation mask type E
//---------------------- INSTRUMENT     ---------------------------------------------
TT23_BSGV_005            : INSTRUMENT  , L := l.TT23_BSGV_005;     ! SEM grid, vertical
TT23_BSI                 : INSTRUMENT  , L := l.TT23_BSI;          ! SEM intensity
TT23_BSM                 : INSTRUMENT  , L := l.TT23_BSM;          ! SEM position, moveable
TT23_BTV                 : INSTRUMENT  , L := l.TT23_BTV;          ! light screen monitor
TT23_BTV__004            : INSTRUMENT  , L := l.TT23_BTV__004;     ! Beam observation TV with screen
TT23_TBIU                : INSTRUMENT  , L := l.TT23_TBIU;         ! target beam instrumentation, upstream
TT23_VVSB                : INSTRUMENT  , L := l.TT23_VVSB;         ! vacuum valve, sector, diameter 150 mm
//---------------------- MARKER         ---------------------------------------------
TT23_OMK                 : MARKER      , L := l.TT23_OMK;          ! TT23 markers
//---------------------- MONITOR        ---------------------------------------------
TT23_BSG                 : MONITOR     , L := l.TT23_BSG;          ! SEM grid, horizontal + vertical
TT23_BSPH_002            : MONITOR     , L := l.TT23_BSPH_002;     ! SEM position, horizontal
TT23_TBID                : MONITOR     , L := l.TT23_TBID;         ! target beam instrumentation, downstream
//---------------------- QUADRUPOLE     ---------------------------------------------
TT23_QTAD                : QUADRUPOLE  , L := l.TT23_QTAD;         ! quadrupole, BT line, long, enlarged aperture, defocussing
TT23_QTAF                : QUADRUPOLE  , L := l.TT23_QTAF;         ! quadrupole, BT line, long, enlarged aperture, focussing
//---------------------- RBEND          ---------------------------------------------
TT23_MBNH_HWP            : RBEND       , L := l.TT23_MBNH_HWP;     ! Bending magnet, secondary beams, horizontal, north area
TT23_MDAH                : RBEND       , L := l.TT23_MDAH;         ! Correcting dipole, BT line, enlarged aperture, horizontal deflection
TT23_MDAV                : RBEND       , L := l.TT23_MDAV;         ! Correcting dipole, BT line, enlarged aperture, vertical deflection
TT23_MTN__HWP            : RBEND       , L := l.TT23_MTN__HWP;     ! Bending magnet, Target N


/************************************************************************************/
/*                      TT23 SEQUENCE                                               */
/************************************************************************************/

TT23 : SEQUENCE, refer = centre,        L = 151.6867;
 VVSB.230107                   : TT23_VVSB              , at = 19.9345      , slot_id = 56895175;
 BSGV.230111                   : TT23_BSGV_005          , at = 26.275       , slot_id = 46651885;
 BSPH.230111                   : TT23_BSPH_002          , at = 26.275       , slot_id = 46651886;
 MDAV.230112                   : TT23_MDAV              , at = 27.68        , slot_id = 46651887;
 MDAV.230116                   : TT23_MDAV              , at = 29.798       , slot_id = 46651888;
 BTV.230122                    : TT23_BTV               , at = 32.826       , slot_id = 46651889;
 QTAD.230200                   : TT23_QTAD              , at = 34.969       , slot_id = 46651890;
 QTAF.230300                   : TT23_QTAF              , at = 47.647       , slot_id = 46651891;
 MDAH.230361                   : TT23_MDAH              , at = 83.7227      , slot_id = 46651892;
 QTAF.230400                   : TT23_QTAF              , at = 86.5787      , slot_id = 46651893;
 QTAF.230500                   : TT23_QTAF              , at = 90.2447      , slot_id = 46651894;
 BSG.230505                    : TT23_BSG               , at = 92.4677      , slot_id = 46651895;
 MDAV.230513                   : TT23_MDAV              , at = 97.5707      , slot_id = 46651896;
 MDAV.230517                   : TT23_MDAV              , at = 99.6887      , slot_id = 46651897;
 QTAD.230600                   : TT23_QTAD              , at = 102.5447     , slot_id = 46651898;
 QTAD.230700                   : TT23_QTAD              , at = 106.2107     , slot_id = 46651899;
 BSI.230705                    : TT23_BSI               , at = 108.4337     , slot_id = 46651900;
 MDAH.230718                   : TT23_MDAH              , at = 116.3547     , slot_id = 46651901;
 QTAF.230800                   : TT23_QTAF              , at = 119.2107     , slot_id = 46651902;
 QTAF.230900                   : TT23_QTAF              , at = 122.8767     , slot_id = 46651903;
 BSM.230905                    : TT23_BSM               , at = 125.0997     , slot_id = 46651904;
 MBN.230907                    : TT23_MBNH_HWP          , at = 127.8997     , slot_id = 46651905;
 BTV.230925                    : TT23_BTV__004          , at = 137.0217     , slot_id = 46651907;
 MTS.230928                    : TT23_MTN__HWP          , at = 139.5367     , slot_id = 57611577, assembly_id= 57611554;
 MTS.230935                    : TT23_MTN__HWP          , at = 143.7367     , slot_id = 57611606, assembly_id= 57611554;
 MTS.230942                    : TT23_MTN__HWP          , at = 147.9367     , slot_id = 57611637, assembly_id= 57611554;
 TCMAE.230948                  : TT23_TCMAE             , at = 150.2117     , slot_id = 57128725;
 TBIU.230949                   : TT23_TBIU              , at = 150.5617     , slot_id = 46651908;
 TBACA.X0200000                : TT23_OMK               , at = 151.0867     , slot_id = 56309981;
 TBID.230950                   : TT23_TBID              , at = 151.5617     , slot_id = 46651909;
 TCMAA.X0200001                : TT23_TCMAA             , at = 151.9367     , slot_id = 56992098;
ENDSEQUENCE;

return;