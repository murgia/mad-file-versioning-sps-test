

  /**********************************************************************************
  *
  * BTP version (draft) EYETS 2024-2025 in MAD X SEQUENCE format
  * Generated the 13-MAR-2024 14:13:31 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.BTP_BCTF_002                 := 0;
l.BTP_BPUIA003                 := 0;
l.BTP_BPUWB                    := 0;
l.BTP_MBHGAWWP                 := 2.000457;
l.BTP_MCXAE002                 := 0.436;
l.BTP_MCXAEWAP                 := 0.436;
l.BTP_MQNCMNWC                 := 0.465;
l.BTP_MQNCUNWP                 := 0.64;
l.BTP_OMK                      := 0;

//---------------------- HKICKER        ---------------------------------------------
BTP_MCXAE002             : HKICKER     , L := l.BTP_MCXAE002;      ! Corrector magnet, H or V, type 4af - horizontal function
//---------------------- MARKER         ---------------------------------------------
BTP_OMK                  : MARKER      , L := l.BTP_OMK;           ! BTP markers
//---------------------- MONITOR        ---------------------------------------------
BTP_BCTF_002             : MONITOR     , L := l.BTP_BCTF_002;      ! Fast Beam Current Transformers (Booster Ejection Lines)
BTP_BPUIA003             : MONITOR     , L := l.BTP_BPUIA003;      ! Beam Position Pick-up Inductive type A
BTP_BPUWB                : MONITOR     , L := l.BTP_BPUWB;         ! Beam Position Pickup, Wideband, type B
//---------------------- QUADRUPOLE     ---------------------------------------------
BTP_MQNCMNWC             : QUADRUPOLE  , L := l.BTP_MQNCMNWC;      ! Quadrupole magnet, 0.4m Smitwater
BTP_MQNCUNWP             : QUADRUPOLE  , L := l.BTP_MQNCUNWP;      ! Quadrupole magnet, type BT-BTP
//---------------------- SBEND          ---------------------------------------------
BTP_MBHGAWWP             : SBEND       , L := l.BTP_MBHGAWWP;      ! Bending magnet, booster TL switching magnet
//---------------------- VKICKER        ---------------------------------------------
BTP_MCXAEWAP             : VKICKER     , L := l.BTP_MCXAEWAP;      ! Corrector magnet, H or V, type 4af


/************************************************************************************/
/*                       BTP SEQUENCE                                                */
/************************************************************************************/

BTP : SEQUENCE, refer = centre,         L = 35.466428373;
 BT.BHZ10                      : BTP_MBHGAWWP           , at = 1.0994955    , slot_id = 5554781;
 BTP.VVS10                     : BTP_OMK                , at = 2.299216     , slot_id = 6029923;
 BTP.STP05.ENTRYFACE           : BTP_OMK                , at = 2.487716     , slot_id = 50336561;
 BTP.STP05                     : BTP_OMK                , at = 2.769716     , slot_id = 50336561;
 BTP.STP10.ENTRYFACE           : BTP_OMK                , at = 3.267516     , slot_id = 5643047;
 BTP.STP10                     : BTP_OMK                , at = 3.549516     , slot_id = 5643047;
 BTP.BPM00                     : BTP_BPUIA003           , at = 4.32812      , slot_id = 5643048;
 BTP.DHZ10                     : BTP_MCXAE002           , at = 5.11862      , slot_id = 5643049;
 BTP.DVT10                     : BTP_MCXAEWAP           , at = 5.59862      , slot_id = 5643050;
 BTP.QNO10                     : BTP_MQNCMNWC           , at = 6.70062      , slot_id = 5643051;
 BTP.QNO20                     : BTP_MQNCUNWP           , at = 11.697462    , slot_id = 5643052;
 BTP.DHZ20                     : BTP_MCXAE002           , at = 12.397462    , slot_id = 5643053;
 BTP.BPM10                     : BTP_BPUIA003           , at = 12.795962    , slot_id = 5643054;
 BTP.DVT20                     : BTP_MCXAEWAP           , at = 13.297462    , slot_id = 5643055;
 BTP.QNO30                     : BTP_MQNCUNWP           , at = 13.997462    , slot_id = 5643056;
 BTP.BPMW15                    : BTP_BPUWB              , at = 16.149512    , slot_id = 42527467;
 BTP.QNO35                     : BTP_MQNCUNWP           , at = 17.34218     , slot_id = 5643058;
 BTP.DHZ30                     : BTP_MCXAE002           , at = 18.69218     , slot_id = 5643059;
 BTP.BPM20                     : BTP_BPUIA003           , at = 19.13268     , slot_id = 5643060;
 BTP.DVT30                     : BTP_MCXAEWAP           , at = 19.57618     , slot_id = 5643061;
 BTP.QNO50                     : BTP_MQNCUNWP           , at = 21.287462    , slot_id = 5643062;
 BTP.BPM30                     : BTP_BPUIA003           , at = 23.31068     , slot_id = 5643067;
 BTP.QNO55                     : BTP_MQNCUNWP           , at = 24.09218     , slot_id = 50336563;
 BTP.DVT40                     : BTP_MCXAEWAP           , at = 26.186462    , slot_id = 5643065;
 BTP.BCT10                     : BTP_BCTF_002           , at = 26.736462    , slot_id = 5643064;
 BTP.QNO60                     : BTP_MQNCUNWP           , at = 27.686462    , slot_id = 5643066;
 BTP.VVS20                     : BTP_OMK                , at = 28.322556    , slot_id = 5643069;
 BTP.DHZ40                     : BTP_MCXAE002           , at = 28.686462    , slot_id = 5643070;
 BTP.DVT50                     : BTP_MCXAEWAP           , at = 29.186462    , slot_id = 5643071;
 BTP.BPM60                     : BTP_BPUIA003           , at = 29.684962    , slot_id = 50336564;
ENDSEQUENCE;

return;