

  /**********************************************************************************
  *
  * DR version (draft) YETS 2021-2022 in MAD X SEQUENCE format
  * Generated the 22-DEC-2023 02:09:25 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.DR_AARFA002                  := 2.028;
l.DR_BTVPL130                  := 0.7;
l.DR_MBHHFHWP                  := 0;
l.DR_MBHHGWWP                  := 0;
l.DR_MBHHHWWP                  := 0;
l.DR_MCCAVWAP                  := 0.31;
l.DR_MCHAAWAP                  := 0;
l.DR_MCXAACAP                  := 0;
l.DR_MCXABCIP                  := 0.4;
l.DR_MCXBBWAP                  := 0;
l.DR_MKKFH005                  := 0;
l.DR_MM4EANWP                  := 0;
l.DR_MQNEMNWP                  := 0;
l.DR_MQNENNWP                  := 0;
l.DR_MQNEONWP                  := 0;
l.DR_MQNEPCWP                  := 0;
l.DR_MQNERNWP                  := 0.7396;
l.DR_MQNFCAWP                  := 0;
l.DR_MQNFDAWP                  := 0;
l.DR_MQNFEAWP                  := 0;
l.DR_MQNFFAWP                  := 0;
l.DR_MQSCANWP                  := 0;
l.DR_MX_BANWP                  := 0;

//---------------------- CORRECTOR      ---------------------------------------------
DR_MCCAVWAP              : CORRECTOR   , L := l.DR_MCCAVWAP;       ! Corrector magnet. H+V, AD electron cooler
DR_MCHAAWAP              : CORRECTOR   , L := l.DR_MCHAAWAP;       ! Corrector magnet, type DHPA laminated
DR_MCXAACAP              : CORRECTOR   , L := l.DR_MCXAACAP;       ! Corrector magnet, H or V, type MCV
DR_MCXABCIP              : CORRECTOR   , L := l.DR_MCXABCIP;       ! Corrector magnet, H or V, type MCVA
DR_MCXBBWAP              : CORRECTOR   , L := l.DR_MCXBBWAP;       ! Corrector magnet, H or V, type SL/MS
//---------------------- HKICKER        ---------------------------------------------
DR_MKKFH005              : HKICKER     , L := l.DR_MKKFH005;       ! Kicker, Fast Horizontal, AD
//---------------------- MONITOR        ---------------------------------------------
DR_BTVPL130              : MONITOR     , L := l.DR_BTVPL130;       ! Beam observation TV Pneumatic Linear, variant 130

//---------------------- MULTIPOLE      ---------------------------------------------
DR_MM4EANWP              : MULTIPOLE   , L := l.DR_MM4EANWP;       ! Multipole magnet, type QN
//---------------------- QUADRUPOLE     ---------------------------------------------
DR_MQNEMNWP              : QUADRUPOLE  , L := l.DR_MQNEMNWP;       ! Quadrupole magnet, type QN 19 turns /coil
DR_MQNENNWP              : QUADRUPOLE  , L := l.DR_MQNENNWP;       ! Quadrupole magnet, type QN 17 turns /coil
DR_MQNEONWP              : QUADRUPOLE  , L := l.DR_MQNEONWP;       ! Quadrupole magnet, type QN 15 turns /coil
DR_MQNEPCWP              : QUADRUPOLE  , L := l.DR_MQNEPCWP;       ! Quadrupole magnet, type QFC54, 0.8m
DR_MQNERNWP              : QUADRUPOLE  , L := l.DR_MQNERNWP;       ! Quadrupole magnet, tpye QN AD injection
DR_MQNFCAWP              : QUADRUPOLE  , L := l.DR_MQNFCAWP;       ! Quadrupole magnet, type QW 20 turns /coil
DR_MQNFDAWP              : QUADRUPOLE  , L := l.DR_MQNFDAWP;       ! Quadrupole magnet, type QW 22 turns /coil
DR_MQNFEAWP              : QUADRUPOLE  , L := l.DR_MQNFEAWP;       ! Quadrupole magnet, type QW 26 turns /coil
DR_MQNFFAWP              : QUADRUPOLE  , L := l.DR_MQNFFAWP;       ! Quadrupole magnet, type QW 17 turns /coil
DR_MQSCANWP              : QUADRUPOLE  , L := l.DR_MQSCANWP;       ! Quadrupole magnet, skew, ISR, type QSK
//---------------------- RFCAVITY       ---------------------------------------------
DR_AARFA002              : RFCAVITY    , L := l.DR_AARFA002;       ! RF Cavity 9.55MHz, AD Ring
//---------------------- SBEND          ---------------------------------------------
DR_MBHHFHWP              : SBEND       , L := l.DR_MBHHFHWP;       ! Bending magnet, type BHW
DR_MBHHGWWP              : SBEND       , L := l.DR_MBHHGWWP;       ! Bending magnet, type BHN, AD
DR_MBHHHWWP              : SBEND       , L := l.DR_MBHHHWWP;       ! Bending magnet, type BHS special injection
//---------------------- SEXTUPOLE      ---------------------------------------------
DR_MX_BANWP              : SEXTUPOLE   , L := l.DR_MX_BANWP;       ! Sextupole Magnet, axis adjutable, ISR


/************************************************************************************/
/*                      DR SEQUENCE                                                 */
/************************************************************************************/

DR : SEQUENCE, refer = centre,          L = 182.25802;
 DR.QFN02                      : DR_MQNEMNWP            , at = 2.9447       , slot_id = 6709092;
 DR.DHZ0204                    : DR_MCXBBWAP            , at = 5.24         , slot_id = 12998927;
 DR.DVT0208                    : DR_MCXBBWAP            , at = 5.825        , slot_id = 12998928;
 DR.QDS03                      : DR_MM4EANWP            , at = 6.2447       , slot_id = 6709102;
 DR.QFN04                      : DR_MQNENNWP            , at = 9.43395      , slot_id = 6709488;
 DR.QDN05                      : DR_MQNEONWP            , at = 12.6302      , slot_id = 6710133;
 DR.BHN05                      : DR_MBHHGWWP            , at = 13.775       , slot_id = 6710316;
 DR.QFW06                      : DR_MQNFCAWP            , at = 16.1229      , slot_id = 6710343;
 DR.BHN06                      : DR_MBHHGWWP            , at = 17.2763      , slot_id = 6710317;
 DR.QDW07                      : DR_MQNFDAWP            , at = 19.62765     , slot_id = 6731273;
 DR.QFW08                      : DR_MQNFEAWP            , at = 22.2281      , slot_id = 6731282;
 DR.BHW08                      : DR_MBHHFHWP            , at = 23.3778      , slot_id = 6731295;
 DR.QDW09                      : DR_MQNFFAWP            , at = 25.72205     , slot_id = 6731466;
 DR.BHW09                      : DR_MBHHFHWP            , at = 26.8791      , slot_id = 6731296;
 DR.QFW10                      : DR_MQNFEAWP            , at = 29.2307      , slot_id = 6731283;
 DR.QDW11                      : DR_MQNFDAWP            , at = 31.83065     , slot_id = 6731274;
 DR.BHN11                      : DR_MBHHGWWP            , at = 32.9806      , slot_id = 6710318;
 DR.QFW12                      : DR_MQNFCAWP            , at = 35.3285      , slot_id = 6710344;
 DR.BHN12                      : DR_MBHHGWWP            , at = 36.4819      , slot_id = 6710319;
 DR.QDN13                      : DR_MQNEONWP            , at = 38.8384      , slot_id = 6710134;
 DR.DVT1304                    : DR_MCXAACAP            , at = 39.9862      , slot_id = 12998930;
 DR.QFN14                      : DR_MQNENNWP            , at = 42.04215     , slot_id = 6709490;
 DR.QSK1404                    : DR_MQSCANWP            , at = 43.0467      , slot_id = 6731471;
 DR.QDS15                      : DR_MM4EANWP            , at = 45.2529      , slot_id = 6709103;
 DR.QFN16                      : DR_MQNENNWP            , at = 48.44215     , slot_id = 6709491;
 DR.XRC1604                    : DR_MX_BANWP            , at = 49.5632      , slot_id = 12998932;
 DR.DVT1608                    : DR_MCXAACAP            , at = 51.163       , slot_id = 12998933;
 DR.QDN17                      : DR_MQNEONWP            , at = 51.6384      , slot_id = 6710136;
 DR.BHN17                      : DR_MBHHGWWP            , at = 52.7832      , slot_id = 6710320;
 DR.QFW18                      : DR_MQNFCAWP            , at = 55.1311      , slot_id = 6710345;
 DR.BHN18                      : DR_MBHHGWWP            , at = 56.2845      , slot_id = 6710321;
 DR.QDW19                      : DR_MQNFDAWP            , at = 58.63585     , slot_id = 6731275;
 DR.QFW20                      : DR_MQNFEAWP            , at = 61.2363      , slot_id = 6731284;
 DR.BHW20                      : DR_MBHHFHWP            , at = 62.386       , slot_id = 6731297;
 DR.QDW21                      : DR_MQNFFAWP            , at = 64.73025     , slot_id = 6731467;
 DR.BHW21                      : DR_MBHHFHWP            , at = 65.8873      , slot_id = 6731459;
 DR.QFW22                      : DR_MQNFEAWP            , at = 68.2389      , slot_id = 6731285;
 DR.QDW23                      : DR_MQNFDAWP            , at = 70.83885     , slot_id = 6731276;
 DR.BHN23                      : DR_MBHHGWWP            , at = 71.9888      , slot_id = 6710322;
 DR.QFW24                      : DR_MQNFCAWP            , at = 74.3367      , slot_id = 6710346;
 DR.BHN24                      : DR_MBHHGWWP            , at = 75.4901      , slot_id = 6710323;
 DR.QDN25                      : DR_MQNEONWP            , at = 77.8466      , slot_id = 6710137;
 DR.CBR2506                    : DR_AARFA002            , at = 79.8189      , slot_id = 6740225;
 DR.QFN26                      : DR_MQNENNWP            , at = 81.05035     , slot_id = 6709493;
 DR.CBR2606                    : DR_AARFA002            , at = 83.0739      , slot_id = 6740226;
 DR.DVT2608                    : DR_MCHAAWAP            , at = 84.9464      , slot_id = 6731480;
 DR.QDN27                      : DR_MQNEMNWP            , at = 85.417       , slot_id = 6709093;
 DR.QDN28                      : DR_MQNEMNWP            , at = 86.36974     , slot_id = 6709094;
 DR.QFN29A                     : DR_MQNEMNWP            , at = 87.43134     , slot_id = 6709095;
 DR.DHV2904                    : DR_MCCAVWAP            , at = 88.5864      , slot_id = 6731481;
 DR.DHZ2908                    : DR_MCXABCIP            , at = 89.2664      , slot_id = 12998935;
 DR.DHZ2913                    : DR_MCXABCIP            , at = 93.1664      , slot_id = 12998936;
 DR.DHV2917                    : DR_MCCAVWAP            , at = 93.8464      , slot_id = 6731482;
 DR.QFN29B                     : DR_MQNEMNWP            , at = 94.29246     , slot_id = 6709096;
 DR.QDN30                      : DR_MQNEMNWP            , at = 95.34346     , slot_id = 6709097;
 DR.QDN31                      : DR_MQNEMNWP            , at = 96.2962      , slot_id = 6709098;
 DR.DVT3105                    : DR_MCHAAWAP            , at = 97.4864      , slot_id = 6731483;
 DR.QFN32                      : DR_MQNENNWP            , at = 100.65035    , slot_id = 6709495;
 DR.QDN33                      : DR_MQNEONWP            , at = 103.8466     , slot_id = 6710138;
 DR.BHN33                      : DR_MBHHGWWP            , at = 104.9914     , slot_id = 6710324;
 DR.QFW34                      : DR_MQNFCAWP            , at = 107.3393     , slot_id = 6710347;
 DR.BHN34                      : DR_MBHHGWWP            , at = 108.4927     , slot_id = 6710336;
 DR.QDW35                      : DR_MQNFDAWP            , at = 110.84405    , slot_id = 6731277;
 DR.QFW36                      : DR_MQNFEAWP            , at = 113.4445     , slot_id = 6731286;
 DR.BHW36                      : DR_MBHHFHWP            , at = 114.5942     , slot_id = 6731460;
 DR.QDW37                      : DR_MQNFFAWP            , at = 116.93845    , slot_id = 6731468;
 DR.BHW37                      : DR_MBHHFHWP            , at = 118.0955     , slot_id = 6731461;
 DR.QFW38                      : DR_MQNFEAWP            , at = 120.4471     , slot_id = 6731287;
 DR.QDW39                      : DR_MQNFDAWP            , at = 123.04705    , slot_id = 6731278;
 DR.BHN39                      : DR_MBHHGWWP            , at = 124.197      , slot_id = 6710337;
 DR.QFW40                      : DR_MQNFCAWP            , at = 126.5449     , slot_id = 6710348;
 DR.BHN40                      : DR_MBHHGWWP            , at = 127.6983     , slot_id = 6710338;
 DR.QDN41                      : DR_MQNEONWP            , at = 130.0548     , slot_id = 6710139;
 DR.XRC4108                    : DR_MX_BANWP            , at = 132.8696     , slot_id = 12998937;
 DR.QFN42                      : DR_MQNENNWP            , at = 133.25855    , slot_id = 6709496;
 DR.QDS43                      : DR_MM4EANWP            , at = 136.4693     , slot_id = 6709104;
 DR.QSK4308                    : DR_MQSCANWP            , at = 138.8761     , slot_id = 6731472;
 DR.QFN44                      : DR_MQNENNWP            , at = 139.65855    , slot_id = 6709497;
 DR.DVT4408                    : DR_MCXAACAP            , at = 142.4386     , slot_id = 12998938;
 DR.QDN45                      : DR_MQNEONWP            , at = 142.8548     , slot_id = 6710140;
 DR.BHN45                      : DR_MBHHGWWP            , at = 143.9996     , slot_id = 6710339;
 DR.QFW46                      : DR_MQNFCAWP            , at = 146.3475     , slot_id = 6710349;
 DR.BHN46                      : DR_MBHHGWWP            , at = 147.5009     , slot_id = 6710340;
 DR.QDW47                      : DR_MQNFDAWP            , at = 149.85225    , slot_id = 6731279;
 DR.QFW48                      : DR_MQNFEAWP            , at = 152.4527     , slot_id = 6731288;
 DR.BHW48                      : DR_MBHHFHWP            , at = 153.6024     , slot_id = 6731462;
 DR.QDW49                      : DR_MQNFFAWP            , at = 155.94665    , slot_id = 6731469;
 DR.BHW49                      : DR_MBHHFHWP            , at = 157.1037     , slot_id = 6731463;
 DR.QFW50                      : DR_MQNFEAWP            , at = 159.4553     , slot_id = 6731289;
 DR.KFE5005                    : DR_MKKFH005            , at = 160.416      , slot_id = 14301521;
 DR.QDW51                      : DR_MQNFDAWP            , at = 162.05525    , slot_id = 6731280;
 DR.BHN51                      : DR_MBHHGWWP            , at = 163.2052     , slot_id = 6710341;
 DR.QFW52                      : DR_MQNFCAWP            , at = 165.5531     , slot_id = 6710350;
 DR.BHS52                      : DR_MBHHHWWP            , at = 166.7065     , slot_id = 6731478;
 DR.QDC53                      : DR_MQNERNWP            , at = 169.063      , slot_id = 6731474;
 DR.BTV5303                    : DR_BTVPL130            , at = 170.37526    , slot_id = 6154994;
 DR.QFC54                      : DR_MQNEPCWP            , at = 172.26675    , slot_id = 6731476;
 DR.DHZ5404                    : DR_MCXBBWAP            , at = 174.0008     , slot_id = 12998939;
 DR.DVT5408                    : DR_MCXBBWAP            , at = 174.6733     , slot_id = 12998940;
 DR.QDS55                      : DR_MM4EANWP            , at = 175.4775     , slot_id = 6709105;
 DR.KFI5506                    : DR_MKKFH005            , at = 176.4913     , slot_id = 14301523;
 DR.QFN56                      : DR_MQNEMNWP            , at = 178.7775     , slot_id = 6709099;
 DR.KFI5606                    : DR_MKKFH005            , at = 179.7318     , slot_id = 14301530;
 DR.QDS01                      : DR_MM4EANWP            , at = 182.25515    , slot_id = 6709101;
ENDSEQUENCE;

return;