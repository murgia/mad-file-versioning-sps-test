

  /**********************************************************************************
  *
  * ETL (LEIR Transfer Line) version (draft) YETS 2021-2022 in MAD X SEQUENCE format
  * Generated the 22-DEC-2023 02:11:32 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.ETL_BCT__004                 := 0;
l.ETL_BPMED                    := 0;
l.ETL_BSGPS                    := 0;
l.ETL_BSGPS002                 := 0;
l.ETL_BTVMR008                 := 0;
l.ETL_BTVMR009                 := 0;
l.ETL_MBHEGHWP                 := 0;
l.ETL_MBXGA002                 := 0;
l.ETL_MCHADWAC                 := 0;
l.ETL_MCXDAHWP                 := 0;
l.ETL_MQNCBNWP                 := 0.385;
l.ETL_MQNDANWP                 := 0;
l.ETL_OMK                      := 0;
l.ETL_UEHV_003                 := 0;

//---------------------- CORRECTOR      ---------------------------------------------
ETL_MCXDAHWP             : CORRECTOR   , L := l.ETL_MCXDAHWP;      ! Corrector magnet, H or V, type MEA43
//---------------------- HKICKER        ---------------------------------------------
ETL_MCHADWAC             : HKICKER     , L := l.ETL_MCHADWAC;      ! Corrector magnet, type DHPA massive
//---------------------- INSTRUMENT     ---------------------------------------------
ETL_BPMED                : INSTRUMENT  , L := l.ETL_BPMED;         ! Beam Position Monitor, Electrostatic, type D
ETL_UEHV_003             : INSTRUMENT  , L := l.ETL_UEHV_003;      ! Beam pick-up (ETL Line)
//---------------------- MARKER         ---------------------------------------------
ETL_OMK                  : MARKER      , L := l.ETL_OMK;           ! ETL markers
//---------------------- MONITOR        ---------------------------------------------
ETL_BCT__004             : MONITOR     , L := l.ETL_BCT__004;      ! Beam current transformer (MTR) (ETL Line type 1)
ETL_BSGPS                : MONITOR     , L := l.ETL_BSGPS;         ! SEM Grid Pneumatic Swing
ETL_BSGPS002             : MONITOR     , L := l.ETL_BSGPS002;      ! SEM Grid Pneumatic Swing (individual wire grid)
ETL_BTVMR008             : MONITOR     , L := l.ETL_BTVMR008;      ! Beam observation TV Motorised Rotating, variant 008
ETL_BTVMR009             : MONITOR     , L := l.ETL_BTVMR009;      ! Beam observation TV Motorised Rotating, variant 009
//---------------------- QUADRUPOLE     ---------------------------------------------
ETL_MQNCBNWP             : QUADRUPOLE  , L := l.ETL_MQNCBNWP;      ! Quadrupole magnet, ISR, type QTN, 0.3m
ETL_MQNDANWP             : QUADRUPOLE  , L := l.ETL_MQNDANWP;      ! Quadrupole magnet, ISR, type QTS, 0.3m
//---------------------- SBEND          ---------------------------------------------
ETL_MBHEGHWP             : SBEND       , L := l.ETL_MBHEGHWP;      ! Bending magnet, type BHN, LEIR
ETL_MBXGA002             : SBEND       , L := l.ETL_MBXGA002;      ! Bending magnet, H or V, type VB4, for ETL transfer line


/************************************************************************************/
/*                       ETL SEQUENCE                                               */
/************************************************************************************/

ETL : SEQUENCE, refer = centre,         L = 60.5;
 ETL.BHN20                     : ETL_MBHEGHWP           , at = -.385227     , slot_id = 6154549;
 ETL.BCT20                     : ETL_BCT__004           , at = .5329        , slot_id = 53156512;
 ETL.BSGH30                    : ETL_BSGPS002           , at = 1.6278       , slot_id = 54855028, assembly_id= 53156482;
 ETL.BSGHV30                   : ETL_BSGPS              , at = 1.6278       , slot_id = 53156482;
 ETL.BSGV30                    : ETL_BSGPS002           , at = 1.6278       , slot_id = 54855051, assembly_id= 53156482;
 ETL.BVN20                     : ETL_MCXDAHWP           , at = 2.3938       , slot_id = 43077854;
 ETL.BPMI60                    : ETL_BPMED              , at = 3.6254       , slot_id = 43137970;
 ETL.BPME60                    : ETL_UEHV_003           , at = 7.4696       , slot_id = 43077876;
 ETL.BTV40                     : ETL_BTVMR009           , at = 8.3561       , slot_id = 43077867;
 ETL.QNN60                     : ETL_MQNDANWP           , at = 9.1561       , slot_id = 6511657;
 ETL.BPMI50                    : ETL_BPMED              , at = 11.235       , slot_id = 43137971;
 ETL.QNN50                     : ETL_MQNDANWP           , at = 12.1563      , slot_id = 6511658;
 ETL.BPME40                    : ETL_UEHV_003           , at = 18.8126      , slot_id = 43077877;
 ETL.BSGH20                    : ETL_BSGPS002           , at = 19.5356      , slot_id = 54854982, assembly_id= 53156467;
 ETL.BSGHV20                   : ETL_BSGPS              , at = 19.5356      , slot_id = 53156467;
 ETL.BSGV20                    : ETL_BSGPS002           , at = 19.5356      , slot_id = 54855005, assembly_id= 53156467;
 ETL.BTV30                     : ETL_BTVMR009           , at = 21.6567      , slot_id = 43077868;
 ETL.BPMI40                    : ETL_BPMED              , at = 22.9128      , slot_id = 43137972;
 ETL.QNN40                     : ETL_MQNCBNWP           , at = 23.6568      , slot_id = 6511807;
 ETL.BPMI30                    : ETL_BPMED              , at = 31.9621      , slot_id = 43137973;
 ETL.QNN30                     : ETL_MQNCBNWP           , at = 32.6572      , slot_id = 6511808;
 ETL.BTV20                     : ETL_BTVMR009           , at = 35.6594      , slot_id = 43077869;
 ETL.BSGH10                    : ETL_BSGPS002           , at = 39.5365      , slot_id = 54854936, assembly_id= 53156452;
 ETL.BSGHV10                   : ETL_BSGPS              , at = 39.5365      , slot_id = 53156452;
 ETL.BSGV10                    : ETL_BSGPS002           , at = 39.5365      , slot_id = 54854959, assembly_id= 53156452;
 ETL.BPMI20                    : ETL_BPMED              , at = 39.9723      , slot_id = 43137974;
 ETL.BPME20                    : ETL_UEHV_003           , at = 42.1797      , slot_id = 43077878;
 ETL.QNN20                     : ETL_MQNDANWP           , at = 43.6577      , slot_id = 6511659;
 ETL.BVN10                     : ETL_MCXDAHWP           , at = 44.5578      , slot_id = 43077858;
 ETL.VVS10                     : ETL_OMK                , at = 49.2896      , slot_id = 43077882;
 ETL.QNN10                     : ETL_MQNCBNWP           , at = 49.9478      , slot_id = 43077859;
 ETL.BHN10                     : ETL_MBXGA002           , at = 51.810025    , slot_id = 6154553;
 ETL.BCT10                     : ETL_OMK                , at = 58.2552      , slot_id = 53156497;
 ETL.BPME00                    : ETL_UEHV_003           , at = 58.7981      , slot_id = 43077879;
 ETL.BTV10                     : ETL_BTVMR008           , at = 59.5513      , slot_id = 43077871;
 ETL.DHN10                     : ETL_MCHADWAC           , at = 60.1609      , slot_id = 43077881;
ENDSEQUENCE;

return;