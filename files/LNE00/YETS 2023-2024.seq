

  /**********************************************************************************
  *
  * LNE00 version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 12-JUN-2023 17:00:45 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE00_BSGWAMEC001            := 0.3;
l.LNE00_VVGBF                  := 0.075;
l.LNE00_VVGBH                  := 0.085;
l.LNE00_ZCH                    := 0.037;
l.LNE00_ZCV                    := 0.037;
l.LNE00_ZDSIA                  := 0.347251;
l.LNE00_ZQ                     := 0.1;
l.LNE00_ZQFD                   := 0.1;
l.LNE00_ZQFF                   := 0.1;
l.LNE00_ZQMD                   := 0.1;
l.LNE00_ZQMF                   := 0.1;
l.LNE00_ZQNA                   := 0.39;

//---------------------- HCORRECTOR     ---------------------------------------------
LNE00_ZCH      : HCORRECTOR  , L := l.LNE00_ZCH;         ! Electrostatic Corrector, Horizontal
//---------------------- INSTRUMENT     ---------------------------------------------
LNE00_BSGWAMEC0: INSTRUMENT  , L := l.LNE00_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
LNE00_VVGBF    : INSTRUMENT  , L := l.LNE00_VVGBF;       ! Vacuum valve gate all metal, electropneumatic controlled, DN63-CFF63 (VAT-S48)
LNE00_VVGBH    : INSTRUMENT  , L := l.LNE00_VVGBH;       ! Vacuum valve gate all metal, electropneumatic controlled, DN100-CFF100 (VAT-S48)
//---------------------- KICKER         ---------------------------------------------
LNE00_ZDSIA    : KICKER      , L := l.LNE00_ZDSIA;       ! Electrostatic Deflector, Slow, for Ions, type A (ELENA)
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE00_ZQ       : QUADRUPOLE  , L := l.LNE00_ZQ;          ! Electrostatic Quadrupoles
LNE00_ZQFD     : QUADRUPOLE  , L := l.LNE00_ZQFD;        ! Electrostatic Quadrupoles, powered in series, Defocussing
LNE00_ZQFF     : QUADRUPOLE  , L := l.LNE00_ZQFF;        ! Electrostatic Quadrupoles, powered in series, Focussing
LNE00_ZQMD     : QUADRUPOLE  , L := l.LNE00_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE00_ZQMF     : QUADRUPOLE  , L := l.LNE00_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE00_ZQNA     : QUADRUPOLE  , Lrad := l.LNE00_ZQNA;        ! Electrostatic Quadrupole, Normal, type A (ELENA)
//---------------------- VCORRECTOR     ---------------------------------------------
LNE00_ZCV      : VCORRECTOR  , L := l.LNE00_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE00 SEQUENCE                                              */
/************************************************************************************/

LNE00 : SEQUENCE, refer = centre,       L = 10.636131;
 LNE.VV.0002                   : LNE00_VVGBH     , at = 1.21387      , slot_id = 34571670;
 LNE.ZQMF.0006                 : LNE00_ZQMF      , at = 2.448        , slot_id = 52057158, assembly_id= 34570194;
 LNE.ZCV.0006                  : LNE00_ZCV       , at = 2.5465       , slot_id = 52057167, assembly_id= 34570194;
 LNE.ZQNA.0006                 : LNE00_ZQNA      , at = 2.5705       , slot_id = 34570194;
 LNE.ZCH.0006                  : LNE00_ZCH       , at = 2.5935       , slot_id = 52057176, assembly_id= 34570194;
 LNE.ZQMD.0007                 : LNE00_ZQMD      , at = 2.692        , slot_id = 52057185, assembly_id= 34570194;
 LNE.BSGWA.0008                : LNE00_BSGWAMEC00, at = 2.9155       , slot_id = 34570195;
 LNE.ZQMF.0013                 : LNE00_ZQMF      , at = 3.698        , slot_id = 52779041, assembly_id= 52779026;
 LNE.ZCV.0013                  : LNE00_ZCV       , at = 3.7965       , slot_id = 52779042, assembly_id= 52779026;
 LNE.ZQNA.0013                 : LNE00_ZQNA      , at = 3.8205       , slot_id = 52779026;
 LNE.ZCH.0013                  : LNE00_ZCH       , at = 3.8435       , slot_id = 52779043, assembly_id= 52779026;
 LNE.ZQMD.0014                 : LNE00_ZQMD      , at = 3.942        , slot_id = 52779044, assembly_id= 52779026;
 LNE.BSGWA.0015                : LNE00_BSGWAMEC00, at = 4.1655       , slot_id = 34571662;
 LNS.ZDSIA.0030                : LNE00_ZDSIA     , at = 4.46445      , slot_id = 34485687;
 LNE.VV.0020                   : LNE00_VVGBF     , at = 5.3301       , slot_id = 52779102;
 LNE.BSGWA.0025                : LNE00_BSGWAMEC00, at = 5.6026       , slot_id = 34571671;
 LNE.ZQFF.0026                 : LNE00_ZQFF      , at = 5.8251       , slot_id = 52782552, assembly_id= 52782537;
 LNE.ZCV.0026                  : LNE00_ZCV       , at = 5.9236       , slot_id = 52782553, assembly_id= 52782537;
 LNE.ZQNA.0026                 : LNE00_ZQNA      , at = 5.9476       , slot_id = 52782537;
 LNE.ZCH.0026                  : LNE00_ZCH       , at = 5.9706       , slot_id = 52782554, assembly_id= 52782537;
 LNE.ZQ.0027                   : LNE00_ZQ        , at = 6.0691       , slot_id = 52782555, assembly_id= 52782537;
 LNE.ZQFD.0032                 : LNE00_ZQFD      , at = 7.3751       , slot_id = 52782627, assembly_id= 52782612;
 LNE.ZCV.0032                  : LNE00_ZCV       , at = 7.4736       , slot_id = 52782628, assembly_id= 52782612;
 LNE.ZQNA.0032                 : LNE00_ZQNA      , at = 7.4976       , slot_id = 52782612;
 LNE.ZCH.0032                  : LNE00_ZCH       , at = 7.5206       , slot_id = 52782629, assembly_id= 52782612;
 LNE.ZQ.0033                   : LNE00_ZQ        , at = 7.6191       , slot_id = 52782630, assembly_id= 52782612;
 LNE.BSGWA.0038                : LNE00_BSGWAMEC00, at = 8.7026       , slot_id = 34571683;
 LNE.ZQFF.0039                 : LNE00_ZQFF      , at = 8.9251       , slot_id = 52782702, assembly_id= 52782687;
 LNE.ZCV.0039                  : LNE00_ZCV       , at = 9.0236       , slot_id = 52782703, assembly_id= 52782687;
 LNE.ZQNA.0039                 : LNE00_ZQNA      , at = 9.0476       , slot_id = 52782687;
 LNE.ZCH.0039                  : LNE00_ZCH       , at = 9.0706       , slot_id = 52782704, assembly_id= 52782687;
 LNE.ZQ.0040                   : LNE00_ZQ        , at = 9.1691       , slot_id = 52782705, assembly_id= 52782687;
 LNE.BSGWA.0045                : LNE00_BSGWAMEC00, at = 10.0086      , slot_id = 34571689;
 LNE.ZQ.0046                   : LNE00_ZQ        , at = 10.2311      , slot_id = 52782777, assembly_id= 52782762;
 LNE.ZCV.0046                  : LNE00_ZCV       , at = 10.3296      , slot_id = 52782778, assembly_id= 52782762;
 LNE.ZQNA.0046                 : LNE00_ZQNA      , at = 10.3536      , slot_id = 52782762;
 LNE.ZCH.0046                  : LNE00_ZCH       , at = 10.3766      , slot_id = 52782779, assembly_id= 52782762;
 LNE.ZQFD.0047                 : LNE00_ZQFD      , at = 10.4751      , slot_id = 52782780, assembly_id= 52782762;
ENDSEQUENCE;

return;